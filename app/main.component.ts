/**
 * Created by mmaksymiuk on 21.01.16.
 */
import {Component, enableProdMode} from 'angular2/core';
import {OnInit} from "../node_modules/angular2/ts/src/core/linker/interfaces";
import {
    FormBuilder,
    Validators,
    Control,
    ControlGroup,
    FORM_DIRECTIVES
} from 'angular2/common';

enableProdMode();

interface ValidationResult {
    [key:string]:boolean;
}

class CustomValidator {
    static isNumber(control:Control):ValidationResult {
        if (isNaN(control.value)) {
            return {"isNumber": true};
        }
        return null;
    }

    static isPositiveNumber(control:Control):ValidationResult {
        if (control.value <= 0) {
            return {"isPositiveNumber": true};
        }
        return null;
    }

    static isLater(group:ControlGroup):ValidationResult {

        let currentHour = new Date().getHours();
        let currentMinute = new Date().getMinutes();
        let endHours = group.controls['hCtrl'].value;
        let endMinutes = group.controls['mCtrl'].value;

        if (endHours <= currentHour && endMinutes <= currentMinute) {
            return {"isLater": true};
        }
        return null;
    }
}

class ConfigData {

    public startDate:Date;
    public endDate:Date;
    public cigaretteCount:number;
    public cigaretteGone:number;
    public duration:number;

    constructor(startDateTimestamp:number,
                endDateTimestamp:number,
                cigaretteCount:number,
                duration?:number,
                cigaretteGone?:number) {
        this.startDate = new Date(startDateTimestamp);
        this.endDate = new Date(endDateTimestamp);
        this.cigaretteCount = cigaretteCount;
        this.cigaretteGone = cigaretteGone;
        this.duration = duration;
    }
}

@Component({
    selector: 'main-app',
    templateUrl: 'app/config-form.component.html',
})

export class MainComponent implements OnInit {

    ngOnInit() {
        this.getConfigData();
        this.setValidators('', '', '');
    }


    configData:ConfigData;

    form:ControlGroup;
    timeCtrl:ControlGroup;
    hCtrl:Control;
    mCtrl:Control;
    cCtrl:Control;


    configRead:boolean;
    periods:number[];
    timer:number = 0;
    dateString:string = "";
    isTime:boolean = false;
    isCigaretteLeft:boolean = false;
    timeoutId:number = 0;
    progressClass:number = 0;

    selectHours:string[] = [
        "00", "01", "02", "03", "04", "05", "06", "07", "08", "09",
        "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
        "20", "21", "22", "23"
    ];
    selectMinutes:string[] = [
        "00", "01", "02", "03", "04", "05", "06", "07", "08", "09",
        "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
        "20", "21", "22", "23", "24", "25", "26", "27", "28", "29",
        "30", "31", "32", "33", "34", "35", "36", "37", "38", "39",
        "40", "41", "42", "43", "44", "45", "46", "47", "48", "49",
        "50", "51", "52", "53", "54", "55", "56", "57", "58", "59"
    ];

    setValidators(c:string, h:string, m:string) {
        this.hCtrl = new Control(h, Validators.required);
        this.mCtrl = new Control(m, Validators.required);
        this.cCtrl = new Control(c, Validators.compose([Validators.required, CustomValidator.isNumber, CustomValidator.isPositiveNumber]));
        var fb:FormBuilder = new FormBuilder();

        this.timeCtrl = fb.group({
            mCtrl: this.mCtrl,
            hCtrl: this.hCtrl
        }, {validator: CustomValidator.isLater});

        this.form = fb.group({
            cCtrl: this.cCtrl,
            timeCtrl: this.timeCtrl
        });
        console.log(this.form);
    }

    getConfigData() {
        if (localStorage.getItem('config')) {
            let config = JSON.parse(localStorage.getItem('config'));
            this.configData = new ConfigData(
                config.startDate,
                config.endDate,
                config.cigaretteCount,
                config.duration,
                config.cigaretteGone
            );
            this.configRead = true;
            this.calculatePeriods();
            this.timer = Math.floor((this.periods[this.configData.cigaretteGone] - new Date().getTime()) / 1000);
            this.startProgress();
            this.isCigaretteLeft = this.configData.cigaretteCount == this.configData.cigaretteGone ? false : true
        } else {
            this.configRead = false;
        }
    }

    startProgress() {

        if (this.timer > 0) {
            this.timeoutId = setTimeout(()=> {
                this.startProgress();
            }, 1000);
            this.timer--;
            this.countProgress();
            this.setDateString();
        } else {
            this.setDateString();
            this.countProgress();
            this.isCigaretteLeft = this.configData.cigaretteCount == this.configData.cigaretteGone ? false : true
        }
    }

    smokeOne() {
        this.configData.cigaretteGone++;
        this.setConfigData(this.configData);
        this.getConfigData();
        this.isCigaretteLeft = this.configData.cigaretteCount == this.configData.cigaretteGone ? false : true
        if(!this.isCigaretteLeft) {
            this.isTime = false;
        }
    }

    resetConfig() {
        this.setValidators(this.configData.cigaretteCount.toString(), this.configData.endDate.getHours().toString(), this.configData.endDate.getMinutes().toString());
        this.deleteConfig();
        this.configRead = false;
    }

    deleteConfig() {
        localStorage.clear();
    }

    setDateString() {
        if (this.timer > 0) {

            this.dateString = "";
            let hours = 0;
            let minutes = 0;
            let seconds = 0;
            if (this.timer > 3600) { //hours
                hours = Math.floor(this.timer / 3600);
            }
            if (this.timer > 60) {
                minutes = Math.floor((this.timer - hours * 3600) / 60);
            }
            if (this.timer > 0) {
                seconds = Math.floor(this.timer - (hours * 3600 + minutes * 60));
            }

            this.dateString += hours > 9 ? hours + "h " : "0" + hours + "h ";
            this.dateString += minutes > 9 ? minutes + "m " : "0" + minutes + "m ";
            this.dateString += seconds > 9 ? seconds + "s" : "0" + seconds + "s";
            this.isTime = false;
        } else {
            this.dateString = "00h 00m 00s";
            this.isTime = true;
        }
    }

    countProgress() {
        if(this.timer > 0) {
            this.progressClass = Math.floor(((this.configData.duration - this.timer*1000)/this.configData.duration).toFixed(2)*100);
        } else {
            this.progressClass = 100;
        }
    }

    setConfigData(configData:ConfigData) {
        localStorage.setItem('config', JSON.stringify(configData));
    }

    calculatePeriods() {
        this.periods = [this.configData.cigaretteCount];
        for (let i = 0; i < this.configData.cigaretteCount; i++) {
            this.periods[i] = this.configData.startDate.getTime() + this.configData.duration * (i + 1);
        }
    }

    /**
     * Submit form
     */
    onSubmit() {

        let curDate:Date = new Date();
        let chosenDate:Date = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate(), this.hCtrl.value, this.mCtrl.value);

        console.log(curDate.getTime());
        console.log(chosenDate.getTime());
        this.configRead = true;
        this.configData = new ConfigData(
            curDate.getTime(),
            chosenDate.getTime(),
            this.cCtrl.value,
            0,
            0
        );
        console.log("timeout id " + this.timeoutId);
        if(this.timeoutId != 0) {
            clearTimeout(this.timeoutId);
        }
        this.doCalculation();
        this.setConfigData(this.configData);

    }

    /**
     * Make some math before save data;
     */
    doCalculation() {
        let dateStart:Date = this.configData.startDate;
        let dateEnd:Date = this.configData.endDate;
        this.configData.duration = (dateEnd.getTime() - dateStart.getTime()) / this.configData.cigaretteCount;
        this.configData.cigaretteGone = 0;
        this.calculatePeriods();
        this.timer = Math.floor((this.periods[this.configData.cigaretteGone] - new Date().getTime()) / 1000);
        console.log("timer set to " + this.timer);
        this.startProgress();
    }


}


