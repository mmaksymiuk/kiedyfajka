System.register(['angular2/core', 'angular2/common'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, common_1;
    var CustomValidator, ConfigData, MainComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            }],
        execute: function() {
            core_1.enableProdMode();
            CustomValidator = (function () {
                function CustomValidator() {
                }
                CustomValidator.isNumber = function (control) {
                    if (isNaN(control.value)) {
                        return { "isNumber": true };
                    }
                    return null;
                };
                CustomValidator.isPositiveNumber = function (control) {
                    if (control.value <= 0) {
                        return { "isPositiveNumber": true };
                    }
                    return null;
                };
                CustomValidator.isLater = function (group) {
                    var currentHour = new Date().getHours();
                    var currentMinute = new Date().getMinutes();
                    var endHours = group.controls['hCtrl'].value;
                    var endMinutes = group.controls['mCtrl'].value;
                    if (endHours <= currentHour && endMinutes <= currentMinute) {
                        return { "isLater": true };
                    }
                    return null;
                };
                return CustomValidator;
            })();
            ConfigData = (function () {
                function ConfigData(startDateTimestamp, endDateTimestamp, cigaretteCount, duration, cigaretteGone) {
                    this.startDate = new Date(startDateTimestamp);
                    this.endDate = new Date(endDateTimestamp);
                    this.cigaretteCount = cigaretteCount;
                    this.cigaretteGone = cigaretteGone;
                    this.duration = duration;
                }
                return ConfigData;
            })();
            MainComponent = (function () {
                function MainComponent() {
                    this.timer = 0;
                    this.dateString = "";
                    this.isTime = false;
                    this.isCigaretteLeft = false;
                    this.timeoutId = 0;
                    this.progressClass = 0;
                    this.selectHours = [
                        "00", "01", "02", "03", "04", "05", "06", "07", "08", "09",
                        "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
                        "20", "21", "22", "23"
                    ];
                    this.selectMinutes = [
                        "00", "01", "02", "03", "04", "05", "06", "07", "08", "09",
                        "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
                        "20", "21", "22", "23", "24", "25", "26", "27", "28", "29",
                        "30", "31", "32", "33", "34", "35", "36", "37", "38", "39",
                        "40", "41", "42", "43", "44", "45", "46", "47", "48", "49",
                        "50", "51", "52", "53", "54", "55", "56", "57", "58", "59"
                    ];
                }
                MainComponent.prototype.ngOnInit = function () {
                    this.getConfigData();
                    this.setValidators('', '', '');
                };
                MainComponent.prototype.setValidators = function (c, h, m) {
                    this.hCtrl = new common_1.Control(h, common_1.Validators.required);
                    this.mCtrl = new common_1.Control(m, common_1.Validators.required);
                    this.cCtrl = new common_1.Control(c, common_1.Validators.compose([common_1.Validators.required, CustomValidator.isNumber, CustomValidator.isPositiveNumber]));
                    var fb = new common_1.FormBuilder();
                    this.timeCtrl = fb.group({
                        mCtrl: this.mCtrl,
                        hCtrl: this.hCtrl
                    }, { validator: CustomValidator.isLater });
                    this.form = fb.group({
                        cCtrl: this.cCtrl,
                        timeCtrl: this.timeCtrl
                    });
                    console.log(this.form);
                };
                MainComponent.prototype.getConfigData = function () {
                    if (localStorage.getItem('config')) {
                        var config = JSON.parse(localStorage.getItem('config'));
                        this.configData = new ConfigData(config.startDate, config.endDate, config.cigaretteCount, config.duration, config.cigaretteGone);
                        this.configRead = true;
                        this.calculatePeriods();
                        this.timer = Math.floor((this.periods[this.configData.cigaretteGone] - new Date().getTime()) / 1000);
                        this.startProgress();
                        this.isCigaretteLeft = this.configData.cigaretteCount == this.configData.cigaretteGone ? false : true;
                    }
                    else {
                        this.configRead = false;
                    }
                };
                MainComponent.prototype.startProgress = function () {
                    var _this = this;
                    if (this.timer > 0) {
                        this.timeoutId = setTimeout(function () {
                            _this.startProgress();
                        }, 1000);
                        this.timer--;
                        this.countProgress();
                        this.setDateString();
                    }
                    else {
                        this.setDateString();
                        this.countProgress();
                        this.isCigaretteLeft = this.configData.cigaretteCount == this.configData.cigaretteGone ? false : true;
                    }
                };
                MainComponent.prototype.smokeOne = function () {
                    this.configData.cigaretteGone++;
                    this.setConfigData(this.configData);
                    this.getConfigData();
                    this.isCigaretteLeft = this.configData.cigaretteCount == this.configData.cigaretteGone ? false : true;
                    if (!this.isCigaretteLeft) {
                        this.isTime = false;
                    }
                };
                MainComponent.prototype.resetConfig = function () {
                    this.setValidators(this.configData.cigaretteCount.toString(), this.configData.endDate.getHours().toString(), this.configData.endDate.getMinutes().toString());
                    this.deleteConfig();
                    this.configRead = false;
                };
                MainComponent.prototype.deleteConfig = function () {
                    localStorage.clear();
                };
                MainComponent.prototype.setDateString = function () {
                    if (this.timer > 0) {
                        this.dateString = "";
                        var hours = 0;
                        var minutes = 0;
                        var seconds = 0;
                        if (this.timer > 3600) {
                            hours = Math.floor(this.timer / 3600);
                        }
                        if (this.timer > 60) {
                            minutes = Math.floor((this.timer - hours * 3600) / 60);
                        }
                        if (this.timer > 0) {
                            seconds = Math.floor(this.timer - (hours * 3600 + minutes * 60));
                        }
                        this.dateString += hours > 9 ? hours + "h " : "0" + hours + "h ";
                        this.dateString += minutes > 9 ? minutes + "m " : "0" + minutes + "m ";
                        this.dateString += seconds > 9 ? seconds + "s" : "0" + seconds + "s";
                        this.isTime = false;
                    }
                    else {
                        this.dateString = "00h 00m 00s";
                        this.isTime = true;
                    }
                };
                MainComponent.prototype.countProgress = function () {
                    if (this.timer > 0) {
                        this.progressClass = Math.floor(((this.configData.duration - this.timer * 1000) / this.configData.duration).toFixed(2) * 100);
                    }
                    else {
                        this.progressClass = 100;
                    }
                };
                MainComponent.prototype.setConfigData = function (configData) {
                    localStorage.setItem('config', JSON.stringify(configData));
                };
                MainComponent.prototype.calculatePeriods = function () {
                    this.periods = [this.configData.cigaretteCount];
                    for (var i = 0; i < this.configData.cigaretteCount; i++) {
                        this.periods[i] = this.configData.startDate.getTime() + this.configData.duration * (i + 1);
                    }
                };
                /**
                 * Submit form
                 */
                MainComponent.prototype.onSubmit = function () {
                    var curDate = new Date();
                    var chosenDate = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate(), this.hCtrl.value, this.mCtrl.value);
                    console.log(curDate.getTime());
                    console.log(chosenDate.getTime());
                    this.configRead = true;
                    this.configData = new ConfigData(curDate.getTime(), chosenDate.getTime(), this.cCtrl.value, 0, 0);
                    console.log("timeout id " + this.timeoutId);
                    if (this.timeoutId != 0) {
                        clearTimeout(this.timeoutId);
                    }
                    this.doCalculation();
                    this.setConfigData(this.configData);
                };
                /**
                 * Make some math before save data;
                 */
                MainComponent.prototype.doCalculation = function () {
                    var dateStart = this.configData.startDate;
                    var dateEnd = this.configData.endDate;
                    this.configData.duration = (dateEnd.getTime() - dateStart.getTime()) / this.configData.cigaretteCount;
                    this.configData.cigaretteGone = 0;
                    this.calculatePeriods();
                    this.timer = Math.floor((this.periods[this.configData.cigaretteGone] - new Date().getTime()) / 1000);
                    console.log("timer set to " + this.timer);
                    this.startProgress();
                };
                MainComponent = __decorate([
                    core_1.Component({
                        selector: 'main-app',
                        templateUrl: 'app/config-form.component.html',
                    }), 
                    __metadata('design:paramtypes', [])
                ], MainComponent);
                return MainComponent;
            })();
            exports_1("MainComponent", MainComponent);
        }
    }
});
//# sourceMappingURL=main.component.js.map