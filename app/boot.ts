import {bootstrap}    from 'angular2/platform/browser'
import {MainComponent} from "./main.component";
import {Type} from "../node_modules/angular2/src/facade/lang";

bootstrap(<Type>MainComponent);